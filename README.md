# 0xLIST

A generic dynamic list implemented with `void*`. It uses arrays instead of linked lists.

### API

- Creating the list. For the creation you ust secify the size of your elements. Here is an example for `int`.

```c
z0_list list;
z0_list_init(&list, sizeof (int));
//...
z0_list_free(&list);
```

- Adding an item to the list. 

```c
int x1;
int x2;
int x3;
int x4;
int x5;
int x6;
int x7;

x1 = 10;
x2 = 20;
x3 = 30;
x4 = 40;
x5 = 50;
x6 = 60;
x7 = 70;

z0_list_add(&list, (void*)&x1, sizeof (int));
z0_list_add(&list, (void*)&x2, sizeof (int));
z0_list_add(&list, (void*)&x3, sizeof (int));
z0_list_add(&list, (void*)&x4, sizeof (int));
z0_list_add(&list, (void*)&x5, sizeof (int));
z0_list_add(&list, (void*)&x6, sizeof (int));
z0_list_add(&list, (void*)&x7, sizeof (int));
 
printf("CAP: %zu\n", list.cap);
printf("LEN: %zu\n", list.len);
for (size_t i = 0; i < list.len; ++i) {
        int   val;
        val = ((int*)list.data)[i];
        printf("[%zu] = %i\n", i, val);
}
```

- Find an item in your list. Returns the pointer to the element if found, otherwise `NULL`.

```c
int key1;
int key2;

key1 = 30;
key2 = 80;

void *ptr1;
void *ptr2;

ptr1 = z0_list_find(&list, (void *)&key1, sizeof (int));
ptr2 = z0_list_find(&list, (void *)&key2, sizeof (int));

int val1;
int val2;

if (ptr1 != NULL) {
        val1 = *(int*)ptr1;
        printf("FOUND: {%i}\n", val1);
} else {
        printf("NOT FOUND\n");
}

if (ptr2 != NULL) {
        val2 = *(int*)ptr2;
        printf("FOUND: {%i}\n", val2);
} else {
        printf("NOT FOUND\n");
}
```

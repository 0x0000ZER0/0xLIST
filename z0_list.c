#include "z0_list.h"

#include <malloc.h>
#include <string.h>
#include <stdint.h>

void
z0_list_init(z0_list *list, size_t size) {
	list->cap  = 4;
	list->len  = 0;
	list->data = malloc(list->cap * size);
}

void
z0_list_add(z0_list *list, void *data, size_t size) {
	if (list->len == list->cap) {
		list->cap  *= 2;
		list->data  = realloc(list->data, list->cap * size);
	}

	void *ptr;
	ptr = (uint8_t*)list->data + size * list->len;

	memcpy(ptr, data, size);
	++list->len;
}

void*
z0_list_find(z0_list *list, void *key, size_t size) {
	void *ptr;
	ptr = list->data;
	
	void *end;
	end = (uint8_t*)list->data + list->len * size;

	int res;
	while(ptr != end) {
		res = memcmp(ptr, key, size);
		if (res == 0)		
			return ptr;

		ptr = (uint8_t*)ptr + size;
	}


	return NULL;
}

void
z0_list_free(z0_list *list) {
	free(list->data);
}

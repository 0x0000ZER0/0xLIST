#ifndef Z0_LIST_H
#define Z0_LIST_H

#include <stddef.h>

typedef struct {
	size_t  cap;
	size_t  len;
	void   *data;
} z0_list;

void
z0_list_init(z0_list*, size_t);

void
z0_list_add(z0_list*, void*, size_t);

void*
z0_list_find(z0_list*, void*, size_t);

void
z0_list_free(z0_list*);

#endif
